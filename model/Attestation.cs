﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach
{
    class Attestation
    {
        private int id;

        private String name;

        private int score;

        public Attestation(int id, string name, int score)
        {
            this.Id = id;
            this.Name = name;
            this.Score = score;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Score { get => score; set => score = value; }

        public Attestation(DataRow datas)
        {
            this.id = int.Parse(datas[0].ToString());
            this.name = datas[1].ToString();
            this.score = int.Parse(datas[2].ToString());
        }
    }
}
