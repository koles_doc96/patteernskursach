﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach
{
    class Student
    {
        private int id;

        private String name;

        private String spec;

        private String stepen;

        public Student(int id, string name, string spec, string stepen)
        {
            this.Id = id;
            this.Name = name;
            this.Spec = spec;
            this.Stepen = stepen;
        }

        public string Spec { get => spec; set => spec = value; }
        public string Name { get => name; set => name = value; }
        public string Stepen { get => stepen; set => stepen = value; }
        public int Id { get => id; set => id = value; }

        public Student(DataRow datas)
        {
            this.id = int.Parse(datas[0].ToString());
            this.name = datas[1].ToString();
            this.spec = datas[2].ToString();
            this.stepen = datas[3].ToString();
        }

        public Student()
        {
        }
    }

}
