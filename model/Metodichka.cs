﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach
{
    class Metodichka
    {
        private int id;

        private String name;

        private String subject;

        private String coauthor;

        public Metodichka(int id, string name, string subject, string coauthor)
        {
            this.Id = id;
            this.Name = name;
            this.Subject = subject;
            this.Coauthor = coauthor;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Subject { get => subject; set => subject = value; }
        public string Coauthor { get => coauthor; set => coauthor = value; }

        public Metodichka(DataRow datas)
        {

                this.id = int.Parse(datas[0].ToString());
                this.name = datas[1].ToString();
                this.subject = datas[2].ToString();
                this.coauthor = datas[3].ToString();
        }
    }
}
