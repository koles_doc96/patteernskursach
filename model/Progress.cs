﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach
{
    class Progress
    {
        private int id;

        private String name;

        private int position;

        public Progress(int id, string name, int position)
        {
            this.Id = id;
            this.Name = name;
            this.Position = position;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public int Position { get => position; set => position = value; }

        public  Progress(DataRow datas)
        {

                this.id = int.Parse(datas[0].ToString());
                this.name = datas[1].ToString();
                this.position = int.Parse(datas[2].ToString());

        }
    }
}
