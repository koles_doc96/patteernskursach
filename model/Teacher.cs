﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach
{
    class Teacher
    {
        private int id;

        private String name;

        private String stepen;

        private String profession;

        public Teacher(int id, string name, string stepen, string profession)
        {
            this.Id = id;
            this.Name = name;
            this.Stepen = stepen;
            this.Profession = profession;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Stepen { get => stepen; set => stepen = value; }
        public string Profession { get => profession; set => profession = value; }
        public Teacher(DataRow datas)
        {

                this.id = int.Parse(datas[0].ToString());
                this.name = datas[1].ToString();
                this.stepen = datas[3].ToString();
                this.profession = datas[2].ToString();
            
        }
    }
}
