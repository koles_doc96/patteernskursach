﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach
{
    class Publish
    {
        private int id;

        private String tema;

        private String place;

        public Publish(int id, string tema, string place)
        {
            this.Id = id;
            this.Tema = tema;
            this.Place = place;
        }

        public int Id { get => id; set => id = value; }
        public string Tema { get => tema; set => tema = value; }
        public string Place { get => place; set => place = value; }

        public Publish(DataRow datas)
        {
                this.id = int.Parse(datas[0].ToString());
                this.tema = datas[1].ToString();
                this.place = datas[2].ToString();
        }
    }
}
