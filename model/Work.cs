﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach
{
    class Work
    {
        private int id;

        private String name;

        private String kind;

        public Work(int id, string name, string kind)
        {
            this.Id = id;
            this.Name = name;
            this.Kind = kind;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Kind { get => kind; set => kind = value; }

        public Work(DataRow datas)
        {
            this.id = int.Parse(datas[0].ToString());
            this.name = datas[1].ToString();
            this.kind = datas[3].ToString();
            
        }
    }
}
