﻿using patternsKursach.dataMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace patternsKursach
{
    public partial class TeacherView : Form
    {
        private TeacherMapper mapper;
        private int idTeacher = -999;
        public TeacherView()
        {
            InitializeComponent();
            StorageAdapter adapter = new StorageAdapter();
            mapper = new TeacherMapper(adapter);
        }


        private void TeacherView_Load(object sender, EventArgs e)
        {
            FillTeacher();
        }

        public void FillTeacher()
        {
            List<Teacher> teachers = mapper.findAll();
            dataGridTeach.Rows.Clear();
            int i = 0;
            foreach (Teacher each in teachers)
            {
                dataGridTeach.Rows.Add();
                dataGridTeach[0, i].Value = each.Id;
                dataGridTeach[1, i].Value = each.Name;
                dataGridTeach[2, i].Value = each.Stepen;
                dataGridTeach[3, i].Value = each.Profession;
                i++;
            }
        }

        public void FillPublish(int id)
        {
            List<Publish> publishes = mapper.findAllPublishTeacher(id);
            dataGridPublish.Rows.Clear();
            int i = 0;
            foreach (Publish each in publishes)
            {
                dataGridPublish.Rows.Add();
                dataGridPublish[0, i].Value = each.Id;
                dataGridPublish[1, i].Value = each.Tema;
                dataGridPublish[2, i].Value = each.Place;
                i++;
            }
        }

        public void FillMethod(int id)
        {
            List<Metodichka> metodichkas = mapper.findAllMetodichkaTeacher(id);
            dataGridMethod.Rows.Clear();
            int i = 0;
            foreach (Metodichka each in metodichkas)
            {
                dataGridMethod.Rows.Add();
                dataGridMethod[0, i].Value = each.Id;
                dataGridMethod[1, i].Value = each.Name;
                dataGridMethod[2, i].Value = each.Subject;
                i++;
            }
        }
        private void DataGridTeach_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Dictionary<String, Object> column = new Dictionary<string, object>();
            column.Add(dataGridTeach.Columns[e.ColumnIndex].Name.ToString(), dataGridTeach[e.ColumnIndex, e.RowIndex].Value);
            mapper.editTeacher(column, int.Parse(dataGridTeach[0, e.RowIndex].Value.ToString()));
            MessageBox.Show("Успешно отредактировано", "Info");

        }
        private String validateTextbox(TextBox textBox)
        {
            if (textBox.Text == "")
            {
                textBox.Focus();
                throw new Exception("Заполните пустые поля");
            }
            return textBox.Text;
        }

        private void ButtonAddTeach_Click(object sender, EventArgs e)
        {
                try
                {
                    String name = validateTextbox(textBoxName);
                    String stepen = validateTextbox(textBoxSt);
                    String prof = validateTextbox(textBoxProf);
                    List<object> column = new List<object>
                {
                    name,
                    prof,
                    stepen
                    
                };
                    mapper.addTeacher(column);
                    FillTeacher();
                    MessageBox.Show("Успешно Добавлено", "Info");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
        }

        private void DataGridTeach_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            tabControl1.SelectedIndex = 1;
            int index = e.RowIndex;
            label1.Text = dataGridTeach[1, e.RowIndex].Value.ToString();
            idTeacher = int.Parse(dataGridTeach[0, index].Value.ToString());
            FillPublish(idTeacher);
            FillMethod(idTeacher);
        }

        private void DataGridPublish_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Dictionary<String, Object> column = new Dictionary<string, object>();
            column.Add(dataGridPublish.Columns[e.ColumnIndex].Name.ToString(), dataGridPublish[e.ColumnIndex, e.RowIndex].Value);
            mapper.editPublish(column, int.Parse(dataGridPublish[0, e.RowIndex].Value.ToString()));
            MessageBox.Show("Успешно отредактировано", "Info");
        }

        private void ButtonAddPublish_Click(object sender, EventArgs e)
        {
            if(idTeacher != -999)
            {
                try
                {
                    String tema = validateTextbox(textBoxTema);
                    String place = validateTextbox(textBoxPlace);
                    List<object> column = new List<object>
                {
                    tema,
                    place,
                    idTeacher
                };
                    mapper.addPublish(column);
                    FillPublish(idTeacher);
                    MessageBox.Show("Успешно Добавлено", "Info");
                    return;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
            MessageBox.Show("Нужно выбрать Преподавателя", "Error");
        }

        private void DataGridMethod_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Dictionary<String, Object> column = new Dictionary<string, object>();
            column.Add(dataGridMethod.Columns[e.ColumnIndex].Name.ToString() == "name_" ? "name" : dataGridMethod.Columns[e.ColumnIndex].Name.ToString(), dataGridMethod[e.ColumnIndex, e.RowIndex].Value);
            mapper.editMetodichka(column, int.Parse(dataGridMethod[0, e.RowIndex].Value.ToString()));
            MessageBox.Show("Успешно отредактировано", "Info");
        }

        private void ButtonAddMetod_Click(object sender, EventArgs e)
        {
            if (idTeacher != -999)
            {
                try
                {
                    String name = validateTextbox(textBoxNameMe);
                    String disc = validateTextbox(textBoxDisc);
                    List<object> column = new List<object>
                {
                    name,
                    disc,
                    "соавтор",
                    idTeacher
                };
                    mapper.addMetodichka(column);
                    FillMethod(idTeacher);
                    MessageBox.Show("Успешно Добавлено", "Info");
                    return;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
            }
            MessageBox.Show("Нужно выбрать Преподавателя", "Error");
        }
    }
}
