﻿namespace patternsKursach
{
    partial class TeacherView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridTeach = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stepen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.prof = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAddTeach = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSt = new System.Windows.Forms.TextBox();
            this.fio = new System.Windows.Forms.Label();
            this.textBoxProf = new System.Windows.Forms.TextBox();
            this.dataGridPublish = new System.Windows.Forms.DataGridView();
            this.id_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tema = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gde = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonAddPublish = new System.Windows.Forms.Button();
            this.textBoxTema = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPlace = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridMethod = new System.Windows.Forms.DataGridView();
            this._id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.discplina = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonAddMetod = new System.Windows.Forms.Button();
            this.textBoxNameMe = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxDisc = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTeach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPublish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMethod)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(818, 471);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.buttonAddTeach);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.textBoxName);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.textBoxSt);
            this.tabPage1.Controls.Add(this.fio);
            this.tabPage1.Controls.Add(this.textBoxProf);
            this.tabPage1.Controls.Add(this.dataGridTeach);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(810, 442);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Преподаватели";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonAddPublish);
            this.tabPage2.Controls.Add(this.textBoxTema);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.textBoxPlace);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.dataGridPublish);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(810, 442);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Публикации";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonAddMetod);
            this.tabPage3.Controls.Add(this.textBoxNameMe);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.textBoxDisc);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.dataGridMethod);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(810, 442);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Методички";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridTeach
            // 
            this.dataGridTeach.AllowUserToAddRows = false;
            this.dataGridTeach.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridTeach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridTeach.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.stepen,
            this.prof});
            this.dataGridTeach.Location = new System.Drawing.Point(6, 6);
            this.dataGridTeach.Name = "dataGridTeach";
            this.dataGridTeach.RowTemplate.Height = 24;
            this.dataGridTeach.Size = new System.Drawing.Size(798, 356);
            this.dataGridTeach.TabIndex = 0;
            this.dataGridTeach.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridTeach_CellEndEdit);
            this.dataGridTeach.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridTeach_CellMouseDoubleClick);
            // 
            // id
            // 
            this.id.HeaderText = "Column1";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // name
            // 
            this.name.HeaderText = "ФИО";
            this.name.Name = "name";
            // 
            // stepen
            // 
            this.stepen.HeaderText = "Ученая степень";
            this.stepen.Name = "stepen";
            // 
            // prof
            // 
            this.prof.HeaderText = "Профориентация";
            this.prof.Name = "prof";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(589, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // buttonAddTeach
            // 
            this.buttonAddTeach.Location = new System.Drawing.Point(603, 382);
            this.buttonAddTeach.Name = "buttonAddTeach";
            this.buttonAddTeach.Size = new System.Drawing.Size(95, 43);
            this.buttonAddTeach.TabIndex = 14;
            this.buttonAddTeach.Text = "Добавить";
            this.buttonAddTeach.UseVisualStyleBackColor = true;
            this.buttonAddTeach.Click += new System.EventHandler(this.ButtonAddTeach_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(424, 379);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Профориентация";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(71, 402);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(166, 22);
            this.textBoxName.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(243, 379);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Ученая степень";
            // 
            // textBoxSt
            // 
            this.textBoxSt.Location = new System.Drawing.Point(243, 402);
            this.textBoxSt.Name = "textBoxSt";
            this.textBoxSt.Size = new System.Drawing.Size(175, 22);
            this.textBoxSt.TabIndex = 9;
            // 
            // fio
            // 
            this.fio.AutoSize = true;
            this.fio.Location = new System.Drawing.Point(71, 379);
            this.fio.Name = "fio";
            this.fio.Size = new System.Drawing.Size(42, 17);
            this.fio.TabIndex = 11;
            this.fio.Text = "ФИО";
            // 
            // textBoxProf
            // 
            this.textBoxProf.Location = new System.Drawing.Point(424, 402);
            this.textBoxProf.Name = "textBoxProf";
            this.textBoxProf.Size = new System.Drawing.Size(172, 22);
            this.textBoxProf.TabIndex = 10;
            // 
            // dataGridPublish
            // 
            this.dataGridPublish.AllowUserToAddRows = false;
            this.dataGridPublish.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridPublish.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridPublish.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_,
            this.tema,
            this.gde});
            this.dataGridPublish.Location = new System.Drawing.Point(6, 6);
            this.dataGridPublish.Name = "dataGridPublish";
            this.dataGridPublish.RowTemplate.Height = 24;
            this.dataGridPublish.Size = new System.Drawing.Size(798, 363);
            this.dataGridPublish.TabIndex = 0;
            this.dataGridPublish.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridPublish_CellEndEdit);
            // 
            // id_
            // 
            this.id_.HeaderText = "Column1";
            this.id_.Name = "id_";
            this.id_.Visible = false;
            // 
            // tema
            // 
            this.tema.HeaderText = "Тема Публикации";
            this.tema.Name = "tema";
            // 
            // gde
            // 
            this.gde.HeaderText = "Место публикации";
            this.gde.Name = "gde";
            // 
            // buttonAddPublish
            // 
            this.buttonAddPublish.Location = new System.Drawing.Point(442, 380);
            this.buttonAddPublish.Name = "buttonAddPublish";
            this.buttonAddPublish.Size = new System.Drawing.Size(95, 43);
            this.buttonAddPublish.TabIndex = 21;
            this.buttonAddPublish.Text = "Добавить";
            this.buttonAddPublish.UseVisualStyleBackColor = true;
            this.buttonAddPublish.Click += new System.EventHandler(this.ButtonAddPublish_Click);
            // 
            // textBoxTema
            // 
            this.textBoxTema.Location = new System.Drawing.Point(62, 401);
            this.textBoxTema.Name = "textBoxTema";
            this.textBoxTema.Size = new System.Drawing.Size(166, 22);
            this.textBoxTema.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(234, 378);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "Место публикации";
            // 
            // textBoxPlace
            // 
            this.textBoxPlace.Location = new System.Drawing.Point(234, 401);
            this.textBoxPlace.Name = "textBoxPlace";
            this.textBoxPlace.Size = new System.Drawing.Size(175, 22);
            this.textBoxPlace.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(62, 378);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 17);
            this.label6.TabIndex = 18;
            this.label6.Text = "Тема публикации";
            // 
            // dataGridMethod
            // 
            this.dataGridMethod.AllowUserToAddRows = false;
            this.dataGridMethod.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridMethod.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMethod.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._id,
            this.name_,
            this.discplina});
            this.dataGridMethod.Location = new System.Drawing.Point(4, 4);
            this.dataGridMethod.Name = "dataGridMethod";
            this.dataGridMethod.RowTemplate.Height = 24;
            this.dataGridMethod.Size = new System.Drawing.Size(803, 369);
            this.dataGridMethod.TabIndex = 0;
            this.dataGridMethod.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridMethod_CellEndEdit);
            // 
            // _id
            // 
            this._id.HeaderText = "Column1";
            this._id.Name = "_id";
            this._id.Visible = false;
            // 
            // name_
            // 
            this.name_.HeaderText = "Название";
            this.name_.Name = "name_";
            // 
            // discplina
            // 
            this.discplina.HeaderText = "Дисциплина";
            this.discplina.Name = "discplina";
            // 
            // buttonAddMetod
            // 
            this.buttonAddMetod.Location = new System.Drawing.Point(472, 381);
            this.buttonAddMetod.Name = "buttonAddMetod";
            this.buttonAddMetod.Size = new System.Drawing.Size(95, 43);
            this.buttonAddMetod.TabIndex = 26;
            this.buttonAddMetod.Text = "Добавить";
            this.buttonAddMetod.UseVisualStyleBackColor = true;
            this.buttonAddMetod.Click += new System.EventHandler(this.ButtonAddMetod_Click);
            // 
            // textBoxNameMe
            // 
            this.textBoxNameMe.Location = new System.Drawing.Point(92, 402);
            this.textBoxNameMe.Name = "textBoxNameMe";
            this.textBoxNameMe.Size = new System.Drawing.Size(166, 22);
            this.textBoxNameMe.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(264, 379);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 17);
            this.label4.TabIndex = 25;
            this.label4.Text = "Дисциплина";
            // 
            // textBoxDisc
            // 
            this.textBoxDisc.Location = new System.Drawing.Point(264, 402);
            this.textBoxDisc.Name = "textBoxDisc";
            this.textBoxDisc.Size = new System.Drawing.Size(175, 22);
            this.textBoxDisc.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(92, 379);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 17);
            this.label7.TabIndex = 24;
            this.label7.Text = "Название";
            // 
            // TeacherView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(843, 496);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Name = "TeacherView";
            this.Text = "Преподаватель";
            this.Load += new System.EventHandler(this.TeacherView_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridTeach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridPublish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMethod)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dataGridTeach;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn stepen;
        private System.Windows.Forms.DataGridViewTextBoxColumn prof;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAddTeach;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSt;
        private System.Windows.Forms.Label fio;
        private System.Windows.Forms.TextBox textBoxProf;
        private System.Windows.Forms.DataGridView dataGridPublish;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_;
        private System.Windows.Forms.DataGridViewTextBoxColumn tema;
        private System.Windows.Forms.DataGridViewTextBoxColumn gde;
        private System.Windows.Forms.Button buttonAddPublish;
        private System.Windows.Forms.TextBox textBoxTema;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPlace;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridMethod;
        private System.Windows.Forms.DataGridViewTextBoxColumn _id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_;
        private System.Windows.Forms.DataGridViewTextBoxColumn discplina;
        private System.Windows.Forms.Button buttonAddMetod;
        private System.Windows.Forms.TextBox textBoxNameMe;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxDisc;
        private System.Windows.Forms.Label label7;
    }
}