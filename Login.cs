﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace patternsKursach
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void StudentBtn_Click(object sender, EventArgs e)
        {
            student student = new student();
            student.ShowDialog();
        }

        private void TeacherBtn_Click(object sender, EventArgs e)
        {
            TeacherView teacher = new TeacherView();
            teacher.ShowDialog();
        }
    }
}
