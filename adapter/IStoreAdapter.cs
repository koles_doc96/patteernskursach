﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach.adapter
{
    interface IStoreAdapter
    {
        DataRow findId(int id, String table);

        DataRow[] findAll(String table);

        DataRow[] findSideKick(String table, String vKey, int id);

        void edit(String table, Dictionary<String, Object> colums, int id);

        void add(String table, List<Object> colums);

        void delete(String table, int id);
        DataRow[] sqlExecute(String sql);

        void AddUpdateExecute(String sql);
    }
}
