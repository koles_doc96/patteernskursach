﻿using MySql.Data.MySqlClient;
using patternsKursach.adapter;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace patternsKursach
{
    class StorageAdapter : IStoreAdapter

    {
        const String dbName = "pattern.";
        private MySqlConnection conn;
        public DataRow findId(int id, String table)
        {
            String sql = $"Select * from {dbName}{table} where id={id.ToString()}";

            return sqlExecute(sql).First();
        }

        public DataRow[] findAll(String table)
        {
            String sql = $"Select * from {dbName}{table}";

            return sqlExecute(sql);
        }

        public DataRow[] findSideKick(String table, String vKey, int id )
        {
            String sql = $"Select *  from {dbName}{table} where {vKey} = {id.ToString()}";

            return sqlExecute(sql);
        }

        public void edit(String table, Dictionary<String, Object> colums, int id)
        {
            String editColumn="";
            foreach (KeyValuePair<String, Object> column in colums)
            {
                String str = column.Value.ToString();
                try
                {
                    int.Parse(str);

                }catch(FormatException ex)
                {
                    str = $"\'{column.Value}\'";
                }
                editColumn += column.Key + "=" + str + ",";
            }

            editColumn = editColumn.Remove(editColumn.Length - 1);

            String sql = $"Update {table} set {editColumn} where id={id}";

            AddUpdateExecute(sql);
        }

        public void add(String table, List<Object> colums)
        {
            String addColumn = "0,";
            foreach (Object each in colums)
            {
                String str = each.ToString();
                if (each is String)
                {
                    str = $"\'{each.ToString()}\'";
                }
                addColumn += str + ",";
            }
            addColumn = addColumn.Remove(addColumn.Length - 1);

            String sql = $"Insert into {dbName}{table} values({addColumn})";

            AddUpdateExecute(sql);
        }

        public void delete(String table, int id)
        {
            String sql = $"Delete from {table} where id={id}";

            AddUpdateExecute(sql);
        }
        public DataRow[] sqlExecute (String sql)
        {
            conn = DbConnection.getConnection();
            MySqlCommand sqlCom = new MySqlCommand(sql, conn);
            conn.Open();
            sqlCom.ExecuteNonQuery();
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter(sqlCom);
            DataTable dt = new DataTable();
            dataAdapter.Fill(dt);
            DataRow[] dataRows = dt.Select();
            conn.Close();

            return dataRows;
        }

        public void AddUpdateExecute(String sql)
        {
            conn = DbConnection.getConnection();
            conn.Open();
            MySqlScript script = new MySqlScript(conn, sql);
            script.Execute();
            conn.Close();
        }

    }
}
