﻿namespace patternsKursach
{
    partial class student
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.workPage = new System.Windows.Forms.TabPage();
            this.attPage = new System.Windows.Forms.TabPage();
            this.progressPage = new System.Windows.Forms.TabPage();
            this.dataGridStudent = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spec = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stepen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxSpec = new System.Windows.Forms.TextBox();
            this.textBoxStepen = new System.Windows.Forms.TextBox();
            this.fio = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonAddStudent = new System.Windows.Forms.Button();
            this.dataGridAttestation = new System.Windows.Forms.DataGridView();
            this.id_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name_ = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ocenka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.textBoxNamePred = new System.Windows.Forms.TextBox();
            this.numericUpOcenka = new System.Windows.Forms.NumericUpDown();
            this.buttonAddAttes = new System.Windows.Forms.Button();
            this.dataGridProgress = new System.Windows.Forms.DataGridView();
            this._id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonAddProgress = new System.Windows.Forms.Button();
            this.numericUpDownMesto = new System.Windows.Forms.NumericUpDown();
            this.textBoxNameProgress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.workPage.SuspendLayout();
            this.attPage.SuspendLayout();
            this.progressPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStudent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAttestation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpOcenka)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProgress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMesto)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.workPage);
            this.tabControl.Controls.Add(this.attPage);
            this.tabControl.Controls.Add(this.progressPage);
            this.tabControl.Location = new System.Drawing.Point(13, 13);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(775, 473);
            this.tabControl.TabIndex = 0;
            // 
            // workPage
            // 
            this.workPage.Controls.Add(this.buttonAddStudent);
            this.workPage.Controls.Add(this.dataGridStudent);
            this.workPage.Controls.Add(this.label3);
            this.workPage.Controls.Add(this.textBoxName);
            this.workPage.Controls.Add(this.label2);
            this.workPage.Controls.Add(this.textBoxSpec);
            this.workPage.Controls.Add(this.fio);
            this.workPage.Controls.Add(this.textBoxStepen);
            this.workPage.Location = new System.Drawing.Point(4, 25);
            this.workPage.Name = "workPage";
            this.workPage.Padding = new System.Windows.Forms.Padding(3);
            this.workPage.Size = new System.Drawing.Size(767, 444);
            this.workPage.TabIndex = 0;
            this.workPage.Text = "Работы";
            // 
            // attPage
            // 
            this.attPage.Controls.Add(this.label4);
            this.attPage.Controls.Add(this.label1);
            this.attPage.Controls.Add(this.buttonAddAttes);
            this.attPage.Controls.Add(this.numericUpOcenka);
            this.attPage.Controls.Add(this.textBoxNamePred);
            this.attPage.Controls.Add(this.dataGridAttestation);
            this.attPage.Location = new System.Drawing.Point(4, 25);
            this.attPage.Name = "attPage";
            this.attPage.Padding = new System.Windows.Forms.Padding(3);
            this.attPage.Size = new System.Drawing.Size(767, 444);
            this.attPage.TabIndex = 1;
            this.attPage.Text = "Аттестация";
            this.attPage.UseVisualStyleBackColor = true;
            // 
            // progressPage
            // 
            this.progressPage.Controls.Add(this.buttonAddProgress);
            this.progressPage.Controls.Add(this.numericUpDownMesto);
            this.progressPage.Controls.Add(this.textBoxNameProgress);
            this.progressPage.Controls.Add(this.dataGridProgress);
            this.progressPage.Location = new System.Drawing.Point(4, 25);
            this.progressPage.Name = "progressPage";
            this.progressPage.Padding = new System.Windows.Forms.Padding(3);
            this.progressPage.Size = new System.Drawing.Size(767, 444);
            this.progressPage.TabIndex = 2;
            this.progressPage.Text = "Достижения";
            this.progressPage.UseVisualStyleBackColor = true;
            // 
            // dataGridStudent
            // 
            this.dataGridStudent.AllowUserToAddRows = false;
            this.dataGridStudent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridStudent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.spec,
            this.stepen});
            this.dataGridStudent.Location = new System.Drawing.Point(7, 7);
            this.dataGridStudent.Name = "dataGridStudent";
            this.dataGridStudent.RowTemplate.Height = 24;
            this.dataGridStudent.Size = new System.Drawing.Size(754, 370);
            this.dataGridStudent.TabIndex = 0;
            this.dataGridStudent.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridStudent_CellContentDoubleClick);
            this.dataGridStudent.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridStudent_CellEndEdit);
            //this.dataGridStudent.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.DataGridStudent_RowsRemoved);
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // name
            // 
            this.name.HeaderText = "Имя";
            this.name.Name = "name";
            // 
            // spec
            // 
            this.spec.HeaderText = "Специальность";
            this.spec.Name = "spec";
            // 
            // stepen
            // 
            this.stepen.HeaderText = "Степень";
            this.stepen.Name = "stepen";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(46, 415);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(166, 22);
            this.textBoxName.TabIndex = 1;
            // 
            // textBoxSpec
            // 
            this.textBoxSpec.Location = new System.Drawing.Point(218, 415);
            this.textBoxSpec.Name = "textBoxSpec";
            this.textBoxSpec.Size = new System.Drawing.Size(175, 22);
            this.textBoxSpec.TabIndex = 2;
            // 
            // textBoxStepen
            // 
            this.textBoxStepen.Location = new System.Drawing.Point(399, 415);
            this.textBoxStepen.Name = "textBoxStepen";
            this.textBoxStepen.Size = new System.Drawing.Size(172, 22);
            this.textBoxStepen.TabIndex = 3;
            // 
            // fio
            // 
            this.fio.AutoSize = true;
            this.fio.Location = new System.Drawing.Point(46, 392);
            this.fio.Name = "fio";
            this.fio.Size = new System.Drawing.Size(42, 17);
            this.fio.TabIndex = 4;
            this.fio.Text = "ФИО";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(218, 392);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Специальность";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(399, 392);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Степень";
            // 
            // buttonAddStudent
            // 
            this.buttonAddStudent.Location = new System.Drawing.Point(578, 395);
            this.buttonAddStudent.Name = "buttonAddStudent";
            this.buttonAddStudent.Size = new System.Drawing.Size(95, 43);
            this.buttonAddStudent.TabIndex = 7;
            this.buttonAddStudent.Text = "Добавить";
            this.buttonAddStudent.UseVisualStyleBackColor = true;
            this.buttonAddStudent.Click += new System.EventHandler(this.ButtonAddStudent_Click);
            // 
            // dataGridAttestation
            // 
            this.dataGridAttestation.AllowUserToAddRows = false;
            this.dataGridAttestation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridAttestation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridAttestation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_,
            this.name_,
            this.ocenka});
            this.dataGridAttestation.Location = new System.Drawing.Point(6, 6);
            this.dataGridAttestation.Name = "dataGridAttestation";
            this.dataGridAttestation.RowTemplate.Height = 24;
            this.dataGridAttestation.Size = new System.Drawing.Size(755, 353);
            this.dataGridAttestation.TabIndex = 0;
            this.dataGridAttestation.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridAttestation_CellEndEdit);
            // 
            // id_
            // 
            this.id_.HeaderText = "id";
            this.id_.Name = "id_";
            this.id_.Visible = false;
            // 
            // name_
            // 
            this.name_.HeaderText = "Предмет";
            this.name_.Name = "name_";
            // 
            // ocenka
            // 
            this.ocenka.HeaderText = "Оценка";
            this.ocenka.Name = "ocenka";
            // 
            // textBoxNamePred
            // 
            this.textBoxNamePred.Location = new System.Drawing.Point(77, 389);
            this.textBoxNamePred.Name = "textBoxNamePred";
            this.textBoxNamePred.Size = new System.Drawing.Size(141, 22);
            this.textBoxNamePred.TabIndex = 1;
            // 
            // numericUpOcenka
            // 
            this.numericUpOcenka.Location = new System.Drawing.Point(235, 389);
            this.numericUpOcenka.Name = "numericUpOcenka";
            this.numericUpOcenka.Size = new System.Drawing.Size(120, 22);
            this.numericUpOcenka.TabIndex = 2;
            // 
            // buttonAddAttes
            // 
            this.buttonAddAttes.Location = new System.Drawing.Point(406, 375);
            this.buttonAddAttes.Name = "buttonAddAttes";
            this.buttonAddAttes.Size = new System.Drawing.Size(107, 49);
            this.buttonAddAttes.TabIndex = 3;
            this.buttonAddAttes.Text = "Добавить";
            this.buttonAddAttes.UseVisualStyleBackColor = true;
            this.buttonAddAttes.Click += new System.EventHandler(this.ButtonAddAttes_Click);
            // 
            // dataGridProgress
            // 
            this.dataGridProgress.AllowUserToAddRows = false;
            this.dataGridProgress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridProgress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridProgress.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._id,
            this._name,
            this.mesto});
            this.dataGridProgress.Location = new System.Drawing.Point(6, 6);
            this.dataGridProgress.Name = "dataGridProgress";
            this.dataGridProgress.RowTemplate.Height = 24;
            this.dataGridProgress.Size = new System.Drawing.Size(755, 355);
            this.dataGridProgress.TabIndex = 0;
            this.dataGridProgress.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridProgress_CellEndEdit);
            // 
            // _id
            // 
            this._id.HeaderText = "Column1";
            this._id.Name = "_id";
            this._id.Visible = false;
            // 
            // _name
            // 
            this._name.HeaderText = "Название";
            this._name.Name = "_name";
            // 
            // mesto
            // 
            this.mesto.HeaderText = "Место";
            this.mesto.Name = "mesto";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 366);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Предмет";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(235, 365);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Оценка";
            // 
            // buttonAddProgress
            // 
            this.buttonAddProgress.Location = new System.Drawing.Point(441, 379);
            this.buttonAddProgress.Name = "buttonAddProgress";
            this.buttonAddProgress.Size = new System.Drawing.Size(107, 49);
            this.buttonAddProgress.TabIndex = 6;
            this.buttonAddProgress.Text = "Добавить";
            this.buttonAddProgress.UseVisualStyleBackColor = true;
            this.buttonAddProgress.Click += new System.EventHandler(this.ButtonAddProgress_Click);
            // 
            // numericUpDownMesto
            // 
            this.numericUpDownMesto.Location = new System.Drawing.Point(270, 393);
            this.numericUpDownMesto.Name = "numericUpDownMesto";
            this.numericUpDownMesto.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownMesto.TabIndex = 5;
            // 
            // textBoxNameProgress
            // 
            this.textBoxNameProgress.Location = new System.Drawing.Point(112, 393);
            this.textBoxNameProgress.Name = "textBoxNameProgress";
            this.textBoxNameProgress.Size = new System.Drawing.Size(141, 22);
            this.textBoxNameProgress.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(582, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "label5";
            // 
            // student
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 498);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tabControl);
            this.Name = "student";
            this.Text = "Студент";
            this.Load += new System.EventHandler(this.Student_Load);
            this.tabControl.ResumeLayout(false);
            this.workPage.ResumeLayout(false);
            this.workPage.PerformLayout();
            this.attPage.ResumeLayout(false);
            this.attPage.PerformLayout();
            this.progressPage.ResumeLayout(false);
            this.progressPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStudent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridAttestation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpOcenka)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridProgress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMesto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage workPage;
        private System.Windows.Forms.TabPage attPage;
        private System.Windows.Forms.TabPage progressPage;
        private System.Windows.Forms.DataGridView dataGridStudent;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn spec;
        private System.Windows.Forms.DataGridViewTextBoxColumn stepen;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxSpec;
        private System.Windows.Forms.TextBox textBoxStepen;
        private System.Windows.Forms.Label fio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonAddStudent;
        private System.Windows.Forms.DataGridView dataGridAttestation;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_;
        private System.Windows.Forms.DataGridViewTextBoxColumn name_;
        private System.Windows.Forms.DataGridViewTextBoxColumn ocenka;
        private System.Windows.Forms.Button buttonAddAttes;
        private System.Windows.Forms.NumericUpDown numericUpOcenka;
        private System.Windows.Forms.TextBox textBoxNamePred;
        private System.Windows.Forms.DataGridView dataGridProgress;
        private System.Windows.Forms.DataGridViewTextBoxColumn _id;
        private System.Windows.Forms.DataGridViewTextBoxColumn _name;
        private System.Windows.Forms.DataGridViewTextBoxColumn mesto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAddProgress;
        private System.Windows.Forms.NumericUpDown numericUpDownMesto;
        private System.Windows.Forms.TextBox textBoxNameProgress;
        private System.Windows.Forms.Label label5;
    }
}