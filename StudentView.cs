﻿using patternsKursach.dataMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace patternsKursach
{
    public partial class student : Form
    {
        private StudentMapper mapper;
        private int idStudent=-999;
        public student()
        {
            InitializeComponent();
            StorageAdapter adapter = new StorageAdapter();
            mapper = new StudentMapper(adapter);
        }

        private void Student_Load(object sender, EventArgs e)
        {
            FillStudent();
        }

        public void FillStudent()
        {
            List<Student> students = mapper.findAll();

            dataGridStudent.Rows.Clear();
            int i = 0;
            foreach (Student student in students)
            {
                dataGridStudent.Rows.Add();
                dataGridStudent[0, i].Value = student.Id;
                dataGridStudent[1, i].Value = student.Name;
                dataGridStudent[2, i].Value = student.Spec;
                dataGridStudent[3, i].Value = student.Stepen;
                i++;
            }
        }

        public void FillAttestation(int id)
        {
            List<Attestation> attestations = mapper.findAllAttestation(id);
            dataGridAttestation.Rows.Clear();
            int i = 0;
            foreach(Attestation each in attestations)
            {
                dataGridAttestation.Rows.Add();
                dataGridAttestation[0, i].Value = each.Id;
                dataGridAttestation[1, i].Value = each.Name;
                dataGridAttestation[2, i].Value = each.Score;
                i++;
            }
        }

        public void FillProgress(int id)
        {
            List<Progress> progresses = mapper.findAllProgres(id);
            dataGridProgress.Rows.Clear();
            int i = 0;
            foreach (Progress each in progresses)
            {
                dataGridProgress.Rows.Add();
                dataGridProgress[0, i].Value = each.Id;
                dataGridProgress[1, i].Value = each.Name;
                dataGridProgress[2, i].Value = each.Position;
                i++;
            }
        }
        private void DataGridStudent_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            tabControl.SelectedIndex = 1;
            idStudent = int.Parse(dataGridStudent[0, e.RowIndex].Value.ToString());
            label5.Text = dataGridStudent[1, e.RowIndex].Value.ToString();
            FillAttestation(idStudent);
            FillProgress(idStudent);
        }

        private void DataGridStudent_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Dictionary<String, Object> column = new Dictionary<string, object>();
            column.Add(dataGridStudent.Columns[e.ColumnIndex].Name.ToString(), dataGridStudent[e.ColumnIndex, e.RowIndex].Value);
            mapper.editStudent(column, int.Parse(dataGridStudent[0, e.RowIndex].Value.ToString()));
            MessageBox.Show( "Успешно отредактировано", "Info");
        }

        private void ButtonAddStudent_Click(object sender, EventArgs e)
        {
            try
            {
                String name = validateTextbox(textBoxName);
                String spec = validateTextbox(textBoxSpec);
                String stepen = validateTextbox(textBoxStepen);
                List<Object> column = new List<object>();
                column.Add(name);
                column.Add(spec);
                column.Add(stepen);
                mapper.addStudent(column);

                FillStudent();
                MessageBox.Show("Успешно Добавлено", "Info");

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

        }

        private String validateTextbox(TextBox textBox)
        {
            if (textBox.Text == "")
            {
                textBox.Focus();
                throw new Exception("Заполните пустые поля");
            }
            return textBox.Text;
        }

        private void DataGridAttestation_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Dictionary<String, Object> column = new Dictionary<string, object>();
            Object value = dataGridAttestation[e.ColumnIndex, e.RowIndex].Value;
            if (dataGridAttestation.Columns[e.ColumnIndex].Name.ToString() == "ocenka")
            {
                try
                {
                    int.Parse(value.ToString());
                }catch(FormatException ex)
                {
                    dataGridAttestation[e.ColumnIndex, e.RowIndex].Value = 0;
                    MessageBox.Show("Введите целое число", "Error");
                    return;
                }
            }
            String name = dataGridAttestation.Columns[e.ColumnIndex].Name.ToString() == "name_" ? "name" : dataGridAttestation.Columns[e.ColumnIndex].Name.ToString();
            column.Add(name, dataGridAttestation[e.ColumnIndex, e.RowIndex].Value);
            mapper.editAttestation(column, int.Parse(dataGridAttestation[0, e.RowIndex].Value.ToString()));
            MessageBox.Show("Успешно отредактировано", "Info");
        }

        private void ButtonAddAttes_Click(object sender, EventArgs e)
        {
            if (idStudent != -999)
            {
                try
            {
                String name = validateTextbox(textBoxNamePred);
                int ocenka = int.Parse(numericUpOcenka.Value.ToString());
                List<object> column = new List<object>
                {
                    name,
                    ocenka,
                    idStudent
                };
                mapper.addAttestation(column);
                FillAttestation(idStudent);
                MessageBox.Show("Успешно Добавлено", "Info");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
                return;
            }
            MessageBox.Show("Нужно выбрать студента", "Error");
        }

        private void DataGridProgress_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Dictionary<String, Object> column = new Dictionary<string, object>();
            Object value = dataGridProgress[e.ColumnIndex, e.RowIndex].Value;
            if (dataGridProgress.Columns[e.ColumnIndex].Name.ToString() == "mesto")
            {
                try
                {
                    int.Parse(value.ToString());
                }
                catch (FormatException ex)
                {
                    dataGridProgress[e.ColumnIndex, e.RowIndex].Value = 0;
                    MessageBox.Show("Введите целое число", "Error");
                    return;
                }
            }

            String name = dataGridProgress.Columns[e.ColumnIndex].Name.ToString() == "_name" ? "name" : dataGridProgress.Columns[e.ColumnIndex].Name.ToString();
                column.Add(name, dataGridProgress[e.ColumnIndex, e.RowIndex].Value);
                mapper.editProgress(column, int.Parse(dataGridProgress[0, e.RowIndex].Value.ToString()));
                MessageBox.Show("Успешно отредактировано", "Info");
        }

        private void ButtonAddProgress_Click(object sender, EventArgs e)
        {
            if (idStudent != -999)
            {
                try
                {
                    String name = validateTextbox(textBoxNameProgress);
                    int mesto = int.Parse(numericUpDownMesto.Value.ToString());
                    List<object> column = new List<object>
                {
                    name,
                    mesto,
                    idStudent
                };
                    mapper.addProgress(column);
                    FillProgress(idStudent);
                    MessageBox.Show("Успешно Добавлено", "Info");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error");
                }
                return;
            }
            MessageBox.Show("Нужно выбрать студента", "Error");
        }

        //private void DataGridStudent_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        //{
        //    try
        //    {
        //        mapper.delete(StudentMapper.studetTable, int.Parse(dataGridStudent[0, e.RowIndex].Value.ToString()));
        //        FillStudent();
        //        MessageBox.Show("Успешно удалено", "Info");
        //    }catch(Exception ex)
        //    {

        //    }

        //}
    }
}
