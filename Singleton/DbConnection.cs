﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace patternsKursach
{
    class DbConnection
    {
        public const String serverName = "localhost";
        public const String userName = "root";
        public const String dbName = "pattern";
        public const String port = "3306";
        public const String password = "";
        public const String connStr = 
            "server=" 
            + serverName +
            ";user=" + userName +
            ";database=" + dbName +
            ";port=" + port +
            ";password=" + password + ";";
        private static MySqlConnection conn;

        private DbConnection()
        {
        }

        public static MySqlConnection getConnection()
        {
            if(conn == null)
            {
                conn = new MySqlConnection(connStr);
            }

            return conn;
        }
    }
}
