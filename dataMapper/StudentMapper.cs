﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach.dataMapper
{
    class StudentMapper
    {
        public static string studetTable = "student";
        public static string workTable = "work";
        public static string studId = "stud_id";
        public static string attestTable = "attestation";
        public static string progressTable = "dostizh";
        private StorageAdapter adapter;
        public StudentMapper(StorageAdapter adapter)
        {
            this.adapter = adapter;
        }

        public void addStudent(List<Object> column)
        {
            adapter.add(studetTable, column);
        }

        public void editStudent(Dictionary<String, Object> columns, int id)
        {
            adapter.edit(studetTable, columns, id);
        }
        public Student findStudentById(int id)
        {
            DataRow data = adapter.findId(id, studetTable);

            return new Student(data);
        }

        public List<Student> findAll()
        {
            DataRow[] datas = adapter.findAll(studetTable);
            List<Student> students = new List<Student>();
            foreach(DataRow data in datas){
                students.Add(new Student(data));
            }
            return students;
        }

        public Work findByWorkId(int id)
        {
            DataRow data = adapter.findId(id, workTable);
            return new Work(data);
        }

        public List<Work> findAllWorkStudent(int id)
        {
            DataRow[] datas = adapter.findSideKick(workTable, studId, id);

            List<Work> works = new List<Work>();
            foreach (DataRow data in datas)
            {
                works.Add(new Work(data));
            }
            return works;
        }
        public void addWork(List<Object> column)
        {
            adapter.add(workTable, column);
        }

        public void editWork(Dictionary<String, Object> columns, int id)
        {
            adapter.edit(workTable, columns, id);
        }
        public List<Attestation> findAllAttestation(int id)
        {
            DataRow[] datas = adapter.findSideKick(attestTable, studId, id);

            List<Attestation> attestation = new List<Attestation>();
            foreach (DataRow data in datas)
            {
                attestation.Add(new Attestation(data));
            }
            return attestation;
        }

        public void addAttestation(List<Object> column)
        {
            adapter.add(attestTable, column);
        }

        public void editAttestation(Dictionary<String, Object> columns, int id)
        {
            adapter.edit(attestTable, columns, id);
        }
        public List<Progress> findAllProgres(int id)
        {
            DataRow[] datas = adapter.findSideKick(progressTable, studId, id);

            List<Progress> progress = new List<Progress>();
            foreach (DataRow data in datas)
            {
                progress.Add(new Progress(data));
            }
            return progress;
        }

        public void addProgress(List<Object> column)
        {
            adapter.add(progressTable, column);
        }

        public void editProgress(Dictionary<String, Object> columns, int id)
        {
            adapter.edit(progressTable, columns, id);
        }

        public void delete(String table, int id)
        {
            adapter.delete(table, id);
        }
    }
}
