﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace patternsKursach.dataMapper
{
    class TeacherMapper
    {
        private const string teacherTable = "teacher";
        private const string publishTable = "publish";
        private const string teachId = "teach_id";
        private const string metodTable = "metodichka";
        private StorageAdapter adapter;

        public TeacherMapper(StorageAdapter adapter)
        {
            this.adapter = adapter;
        }

        public void addTeacher(List<Object> column)
        {
            adapter.add(teacherTable, column);
        }

        public void editTeacher(Dictionary<String, Object> columns, int id)
        {
            adapter.edit(teacherTable, columns, id);
        }
        public Teacher findTeacherById(int id)
        {
            DataRow data = adapter.findId(id, teacherTable);

            return new Teacher(data);
        }

        public List<Teacher> findAll()
        {
            DataRow[] datas = adapter.findAll(teacherTable);
            List<Teacher> teacher = new List<Teacher>();
            foreach (DataRow data in datas)
            {
                teacher.Add(new Teacher(data));
            }
            return teacher;
        }

        public void addPublish(List<Object> column)
        {
            adapter.add(publishTable, column);
        }

        public void editPublish(Dictionary<String, Object> columns, int id)
        {
            adapter.edit(publishTable, columns, id);
        }

        public List<Publish> findAllPublishTeacher(int id)
        {
            DataRow[] datas = adapter.findSideKick(publishTable, teachId, id);

            List <Publish> publish = new List<Publish>();
            foreach (DataRow data in datas)
            {
                publish.Add(new Publish(data));
            }
            return publish;
        }

        public void addMetodichka(List<Object> column)
        {
            adapter.add(metodTable, column);
        }

        public void editMetodichka(Dictionary<String, Object> columns, int id)
        {
            adapter.edit(metodTable, columns, id);
        }

        public List<Metodichka> findAllMetodichkaTeacher(int id)
        {
            DataRow[] datas = adapter.findSideKick(metodTable, teachId, id);

            List<Metodichka> metodichka = new List<Metodichka>();
            foreach (DataRow data in datas)
            {
                metodichka.Add(new Metodichka(data));
            }
            return metodichka;
        }
    }
}
