﻿namespace patternsKursach
{
    partial class Login
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.studentBtn = new System.Windows.Forms.Button();
            this.teacherBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // studentBtn
            // 
            this.studentBtn.Location = new System.Drawing.Point(12, 43);
            this.studentBtn.Name = "studentBtn";
            this.studentBtn.Size = new System.Drawing.Size(123, 36);
            this.studentBtn.TabIndex = 0;
            this.studentBtn.Text = "Студенты";
            this.studentBtn.UseVisualStyleBackColor = true;
            this.studentBtn.Click += new System.EventHandler(this.StudentBtn_Click);
            // 
            // teacherBtn
            // 
            this.teacherBtn.Location = new System.Drawing.Point(158, 43);
            this.teacherBtn.Name = "teacherBtn";
            this.teacherBtn.Size = new System.Drawing.Size(131, 36);
            this.teacherBtn.TabIndex = 1;
            this.teacherBtn.Text = "Преподаватели";
            this.teacherBtn.UseVisualStyleBackColor = true;
            this.teacherBtn.Click += new System.EventHandler(this.TeacherBtn_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 105);
            this.Controls.Add(this.teacherBtn);
            this.Controls.Add(this.studentBtn);
            this.Name = "Login";
            this.Text = "Вход";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button studentBtn;
        private System.Windows.Forms.Button teacherBtn;
    }
}

